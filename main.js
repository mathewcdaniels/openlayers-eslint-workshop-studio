/* global document, navigator */

import 'ol/ol.css';
import DragDrop from 'ol/interaction/draganddrop';
import Draw from 'ol/interaction/draw';
import GeoJSON from 'ol/format/geojson';
import Map from 'ol/map';
import Modify from 'ol/interaction/modify';
import proj from 'ol/proj';
import View from 'ol/view';
// import TileLayer from 'ol/layer/tile';
// import XYZSource from 'ol/source/xyz';
import VectorLayer from 'ol/layer/vector';
import VectorSource from 'ol/source/vector';
import Feature from 'ol/feature';
import Overlay from 'ol/overlay';
import coordinate from 'ol/coordinate';
import Point from 'ol/geom/point';
import Style from 'ol/style/style';
import IconStyle from 'ol/style/icon';
import Snap from 'ol/interaction/snap';
import sync from 'ol-hashed';
import Fill from 'ol/style/fill';
import Stroke from 'ol/style/stroke';
import colormap from 'colormap';
import sphere from 'ol/sphere';

const min = 1e8;
const max = 2e13;
const steps = 50;
const ramp = colormap({
  colormap: 'blackbody',
  nshades: steps,
});

function clamp(value, low, high) {
  return Math.max(low, Math.min(value, high));
}

function getColor(feature) {
  const area = sphere.getArea(feature.getGeometry());
  const f = clamp((area - min) / (max - min), 0, 1) ** (1 / 2);
  const index = Math.round(f * (steps - 1));
  return ramp[index];
}

const baseStyle = function (feature) {
  return new Style({
    fill: new Fill({
      color: getColor(feature),
    }),
    stroke: new Stroke({
      color: 'rbga:(255,255,255,0.8',
    }),
  });
};

const overlay = new Overlay({
  element: document.getElementById('popup-container'),
  positioning: 'bottom-center',
  offset: [0, -10],
});

const modSource = new VectorSource();

const modLayer = new VectorLayer({
  source: modSource,
});

const map = new Map({
  target: 'map-container',
  layers: [
    new VectorLayer({
      source: new VectorSource({
        format: new GeoJSON(),
        url: './data/countries.json',
      }),
      style: baseStyle,
    }),
  ],
  view: new View({
    center: [0, 0],
    zoom: 2,
  }),
});

map.addOverlay(overlay);
map.addLayer(modLayer);

map.on('click', (e) => {
  overlay.setPosition();
  const features = map.getFeaturesAtPixel(e.pixel);
  if (features) {
    const coords = features[0].getGeometry().getCoordinates();
    const hdms = coordinate.toStringHDMS(proj.toLonLat(coords));
    overlay.getElement().innerHTML = hdms;
    overlay.setPosition(coords);
  }
});

const position = new VectorSource();

const vector = new VectorLayer({
  source: position,
});

vector.setStyle(new Style({
  image: new IconStyle({
    src: './data/marker.png',
  }),
}));

map.addLayer(vector);

navigator.geolocation.getCurrentPosition((pos) => {
  const coords = proj.fromLonLat([pos.coords.longitude, pos.coords.latitude]);
  map.getView().animate({ center: coords, zoom: 10 });
  position.addFeature(new Feature(new Point(coords)));
});

map.addInteraction(new DragDrop({
  source: modSource,
  formatConstructors: [GeoJSON],
}));

map.addInteraction(new Draw({
  type: 'Polygon',
  source: modSource,
}));

map.addInteraction(new Snap({
  source: modSource,
}));

map.addInteraction(new Modify({
  source: modSource,
}));

const clear = document.getElementById('clear');
clear.addEventListener('click', () => {
  modSource.clear();
});

const format = new GeoJSON({ featureProjection: 'EPSG:3857' });
const download = document.getElementById('download');
modSource.on('change', () => {
  const features = modSource.getFeatures();
  const json = format.writeFeatures(features);
  download.href = `data:text/json;charset=utf-8,${json}`;
});

sync(map);
